const pro=true;
const janji = new Promise((resolve, reject) =>{
    if (pro) {
        resolve("Janji di tepati")
    } else {
        reject("Janji di ingkari")
    }
})

janji
.finally("Ini proses")
.then((data) => console.log(data))
.catch((error) => console.log(error));